$(function() {
	var priceSlider = document.getElementById("PriceRange");

	noUiSlider.create(priceSlider, {
		start: [15000, 78000],
		connect: true,
		range: {
			'min': 0,
			'max': 120000
		},
		tooltips: true,
		format: wNumb({
			decimals: 0,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 12000,
  			stepped: true
		}
	});
	
	priceSlider.noUiSlider.on('slide', function (currentLabels) {
		currentLabels.forEach(function(item, index) {
			if (parseFloat(item) > 105) {
				$('#PriceRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '50px' });
			} else {
				$('#PriceRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '0px' });
			}
		});		
	});
});