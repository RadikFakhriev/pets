$(function () {
	var fixedBlocksHeight = 710;

	if ($('body').height() < fixedBlocksHeight) {
		$('.pets-house__main').parent().removeClass('col-xl-5').addClass('col-xl-12');
		$('.pets-house__column').parent().removeClass('col-xl-7').addClass('col-xl-12');

		$('.pets-house__main').css({'position': 'initial', 'width' : 'auto' });
		$('.pets-house__population').css({'float': 'none', 'margin-left' : 'auto', 'margin-right' : 'auto' });
		$('.pets-house__column').css({ 'margin-top' : '20px' });
	}
});