$(function () {

	ymaps.ready(function () {
		const map = new ymaps.Map('HouseMap', {
			center: [55.76, 37.64],
			zoom: 11
		});
		
		// Сдвинем карту на 200 пикселей влево
		var position = map.getGlobalPixelCenter();
		map.setGlobalPixelCenter([ position[0] + 200, position[1] ]);

		var placemark = new ymaps.Placemark([55.5813,37.6164], { 
			hintContent: 'Питомник!', 
		}, {
			iconLayout: 'default#image',
			// Своё изображение иконки метки.
			iconImageHref: "./img/marker.png",
			iconImageSize: [87, 66],
			iconImageOffset: [-5, -60]
		});

		map.geoObjects.add(placemark);
	})
	.catch(error => console.log('Failed to load Yandex Maps', error));

	$(".pets-house__main, .pets-house__photo-thumb").on('click', function (e) {
		$.fancybox.open($(".gallery-full-size .fancybox"), { 
			loop: false,
			thumbs : {
				autoStart : true
			}
		});
	});
});