$(function () {
	$(".breed-photos__main, .breed-photos__thumb").on('click', function (e) {
		$.fancybox.open($(".gallery-full-size .fancybox"), { 
			loop: false,
			thumbs : {
				autoStart : true
			}
		});
	});
});