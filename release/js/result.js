$(function () {

	var priceSlider = document.getElementById("PriceRange");
  	var growthSlider = document.getElementById("GrowthRange");
  	var weightSlider = document.getElementById("WeightRange");

  	var priceInitVals = [$('#PriceMin').val(), $('#PriceMax').val()];
  	var growthInitVals = [$('#GrowthMin').val(), $('#GrowthMax').val()];
  	var weightInitVals = [$('#WeightMin').val(), $('#WeightMax').val()];

	noUiSlider.create(priceSlider, {
		start: priceInitVals,
		connect: true,
		range: {
			'min': 0,
			'max': 120000
		},
		tooltips: true,
		format: wNumb({
			decimals: 0,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 12000,
			stepped: true
		}
	});

	noUiSlider.create(growthSlider, {
		start: growthInitVals,
		connect: true,
		range: {
			'min': 0,
			'max': 156
		},
		tooltips: true,
		format: wNumb({
			decimals: 0,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 156,
			stepped: true
		}
	});

	noUiSlider.create(weightSlider, {
		start: weightInitVals,
		connect: true,
		range: {
			'min': 0,
			'max': 24
		},
		tooltips: true,
		format: wNumb({
			decimals: 1,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 100,
			stepped: true
		}
	});

	$('.pets-result-list').on('click', '.pet-card', function (e) {
		var $card = $(e.target).closest('.pet-card');

		if ($card.find('.pet-card__checkbox').is(':checked')) {
			$card.addClass('pet-card--selected');
		} else {
			$card.removeClass('pet-card--selected');
		}
	});

	var priceSliderHandler = function (currentLabels) {
		$('#PriceMin').val(currentLabels[0]);
		$('#PriceMax').val(currentLabels[1]);
		currentLabels.forEach(function(item, index) {
			if (parseFloat(item) > 105) {
				$('#PriceRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '50px' });
			} else {
				$('#PriceRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '0px' });
			}
		});		
	}
	
	var GrowthSliderHandler = function (currentLabels) {
		$('#GrowthMin').val(currentLabels[0]);
		$('#GrowthMax').val(currentLabels[1]);
		currentLabels.forEach(function(item, index) {
			if (parseFloat(item) > 140) {
				$('#GrowthRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '20px' });
			} else {
				$('#GrowthRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '0px' });
			}
		});		
	}
	
	var weightSliderHandler = function (currentLabels) {
		$('#WeightMin').val(currentLabels[0]);
		$('#WeightMax').val(currentLabels[1]);
		currentLabels.forEach(function(item, index) {
			if (parseInt(item) > 22) {
				$('#WeightRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '20px' });
			} else {
				$('#WeightRange').find('.noUi-tooltip').eq(index).css({ 'padding-right' : '0px' });
			}
		});		
	}

	priceSlider.noUiSlider.on('slide', priceSliderHandler);
	priceSlider.noUiSlider.on('set', priceSliderHandler);
	
	growthSlider.noUiSlider.on('slide', GrowthSliderHandler);
	growthSlider.noUiSlider.on('set', GrowthSliderHandler);
	
	weightSlider.noUiSlider.on('slide', weightSliderHandler);
	weightSlider.noUiSlider.on('set', weightSliderHandler);

	$("#ResetFilters").on('click', function (e) {
		e.preventDefault();
		priceSlider.noUiSlider.set([0, 120000]);
		growthSlider.noUiSlider.set([0, 156]);
		weightSlider.noUiSlider.set([0, 24]);
	});
	
	$('.pets-result-list').on('click', '.pet-card', function (e) {
		if ($(e.target).is('.pet-card__selection > label')) {
			setTimeout(function() {
				if ($('.pet-card__checkbox:checked').length > 1) {
					$('.pets-result-list__compare-btn').prop('disabled', false);
				} else {
					$('.pets-result-list__compare-btn').prop('disabled', true);
				}
			}, 200);
		}
	});
});