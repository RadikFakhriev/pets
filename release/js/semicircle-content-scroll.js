$(function () {
	$('.scrollbar-outer').scrollbar();

	$(window).on('resize', function () {
		var screenHeight = $(window).height();
		var screenWidtn = $(window).width();
		var semiContentHeight = $('.semicircle-column').height();
		var estimatedContentHeight = (screenHeight - 220);

		if (estimatedContentHeight > semiContentHeight + 10 ) {
			$('.scroll-element.scroll-y').hide();
			$('.semicircle-column').css({ 'justify-content' : 'center' });
		} else if (estimatedContentHeight < semiContentHeight) {
			$('.scroll-element.scroll-y').show();
			$('.semicircle-column').css({ 'justify-content' : 'unset' });
		}



		$('.semicircle-column').height(Math.abs(estimatedContentHeight));
	});
	
	$(window).trigger('resize');
});