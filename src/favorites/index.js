// Bootstrap dependencies
window.$ = window.jQuery = require('jquery'); // required for bootstrap
window.Popper = require('popper.js'); // required for tooltip, popup...
require('bootstrap');
window.noUiSlider = require('nouislider');
window.wNumb = require('wNumb');

import './index.scss' // include bootstrap css file with own modifications

// tooltip and popover require javascript side modification to enable them (new in Bootstrap 4)
// use tooltip and popover components everywhere
$(function (){
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();

	var priceSlider = document.getElementById("PriceRange");

	noUiSlider.create(priceSlider, {
		start: [15000, 78000],
		connect: true,
		range: {
			'min': 0,
			'max': 120000
		},
		tooltips: true,
		format: wNumb({
			decimals: 0,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 12000,
  			stepped: true
		}
	});

});