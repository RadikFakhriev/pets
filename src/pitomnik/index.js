// Bootstrap dependencies
window.$ = window.jQuery = require('jquery') // required for bootstrap
window.Popper = require('popper.js') // required for tooltip, popup...
require('bootstrap');
import ymaps from 'ymaps';

import './index.scss' // include bootstrap css file with own modifications

// tooltip and popover require javascript side modification to enable them (new in Bootstrap 4)
// use tooltip and popover components everywhere
$(function (){
	$('[data-toggle="tooltip"]').tooltip();
	$('[data-toggle="popover"]').popover();

	ymaps.load('https://api-maps.yandex.ru/2.1/?lang=ru_RU').then(maps => {
		const map = new maps.Map('HouseMap', {
			center: [55.76, 37.64],
			zoom: 11
		});
		
		// Сдвинем карту на 200 пикселей влево
		var position = map.getGlobalPixelCenter();
		map.setGlobalPixelCenter([ position[0] + 200, position[1] ]);

		var placemark = new maps.Placemark([55.5813,37.6164], { 
			hintContent: 'Питомник!', 
		}, {
			iconLayout: 'default#image',
			// Своё изображение иконки метки.
			iconImageHref: "./img/marker.png",
			iconImageSize: [87, 66],
			iconImageOffset: [-5, -60]
		});

		map.geoObjects.add(placemark);
	})
	.catch(error => console.log('Failed to load Yandex Maps', error));


	$(".pets-house__photo-thumb").on('click', function (e) {
		$.fancybox.open($(".gallery-full-size img"), { loop: true });
	});
})