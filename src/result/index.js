// Bootstrap dependencies
window.$ = window.jQuery = require('jquery') // required for bootstrap
window.Popper = require('popper.js') // required for tooltip, popup...
require('bootstrap')
window.noUiSlider = require('nouislider');
window.wNumb = require('wNumb');
require('../semicircle-content-scroll.js');

import './index.scss' // include bootstrap css file with own modifications

// tooltip and popover require javascript side modification to enable them (new in Bootstrap 4)
// use tooltip and popover components everywhere
$(function (){
  $('[data-toggle="tooltip"]').tooltip()
  $('[data-toggle="popover"]').popover()

  	var priceSlider = document.getElementById("PriceRange");
  	var growthSlider = document.getElementById("GrowthRange");
  	var weightSlider = document.getElementById("WeightRange");

	noUiSlider.create(priceSlider, {
		start: [15000, 78000],
		connect: true,
		range: {
			'min': 0,
			'max': 120000
		},
		tooltips: true,
		format: wNumb({
			decimals: 0,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 12000,
			stepped: true
		}
	});

	noUiSlider.create(growthSlider, {
		start: [12, 114],
		connect: true,
		range: {
			'min': 0,
			'max': 156
		},
		tooltips: true,
		format: wNumb({
			decimals: 0,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 156,
			stepped: true
		}
	});

	noUiSlider.create(weightSlider, {
		start: [1.7, 4.8],
		connect: true,
		range: {
			'min': 0,
			'max': 24
		},
		tooltips: true,
		format: wNumb({
			decimals: 1,
			thousand: ' '
		}),
		pips: {
			mode: 'range',
			density: 100,
			stepped: true
		}
	});

	$('.pets-result-list').on('click', '.pet-card', function (e) {
		var $card = $(e.target).closest('.pet-card');

		if ($card.find('.pet-card__checkbox').is(':checked')) {
			$card.addClass('pet-card--selected');
		} else {
			$card.removeClass('pet-card--selected');
		}
	});

});