require('./jquery.scrollbar.min.js');

$(function () {
// 660px

	$('.scrollbar-inner').scrollbar();

	$(window).on('resize', function () {
		var screenHeight = $(window).height();
		var semiContentHeight = $('.semicircle-column').height();
		var estimatedContentHeight = (screenHeight - 220);

		if (estimatedContentHeight > semiContentHeight + 10) {
			$('.scroll-element.scroll-y').hide();
			$('.semicircle-column__content').css({ 'padding-right' : '0px' });
		} else {
			$('.scroll-element.scroll-y').show();
			$('.semicircle-column__content').css({ 'padding-right' : '20px' });
		}

		$('.semicircle-column').height(Math.abs(estimatedContentHeight));
		$('.semicircle-column').find('.scroll-content').css({'max-height' : estimatedContentHeight });


	});
	
	$(window).trigger('resize');

});